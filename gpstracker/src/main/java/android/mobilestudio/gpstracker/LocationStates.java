package android.mobilestudio.gpstracker;

import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;

import static android.content.Context.LOCATION_SERVICE;

class LocationStates implements ILocationStates {

    // The minimum distance to change Updates in meters
    private long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // The minimum time between updates in milliseconds
    private long MIN_TIME_BW_UPDATES = 1000; // 1 minute //1000 msec
    // Declaring a LocationStates Manager
    private LocationManager locationManager;
    private LocationListener listener;

    LocationStates(Context context , LocationListener listener) {
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        this.listener = listener ;
    }

    @Override
    public void startTracker() throws SecurityException {
         startTracking(MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES);
    }

    @Override
    public void startTrackerWithMinTime(long minTime) throws SecurityException {
        startTracking(minTime,MIN_DISTANCE_CHANGE_FOR_UPDATES);
    }

    @Override
    public void startTrackerWithMinDistance(long minDistance) throws SecurityException {
        startTracking(MIN_TIME_BW_UPDATES,minDistance);
    }

    @Override
    public void startTrackerWithMinTimeAndDistance( long minTime, long minDistance) throws SecurityException {
        startTracking(minTime,minDistance);
    }

    @Override
    public void stopTracker() throws SecurityException {
        if (locationManager != null && listener != null) {
            locationManager.removeUpdates(listener);
        }
    }
    private boolean isProviderEnabled() {
        return (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
    }
    void startTracking(long minTime,long minDistance) throws SecurityException{
        if (locationManager != null) {
            if (isProviderEnabled()) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        minTime,
                        minDistance, listener);
            }
        }
    }

}
