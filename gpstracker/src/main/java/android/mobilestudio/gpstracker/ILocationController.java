package android.mobilestudio.gpstracker;

public interface ILocationController {
    void stopTracking();

    void startTracking();

    void startTrackerWithMinTime(long minTime);

    void startTrackerWithMinDistance(long minDistance);

    void startTrackerWithMinTimeAndDistance(long minTime, long minDistance);

}

