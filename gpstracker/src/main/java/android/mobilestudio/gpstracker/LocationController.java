package android.mobilestudio.gpstracker;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

public class LocationController implements LocationListener , ILocationController{

    private LocationUpdateListener locationUpdate;
    private ILocationStates locationState;


    public LocationController(LocationUpdateListener locationUpdateListener, Context context) {
        this.locationUpdate = locationUpdateListener;
        locationState = new LocationStates(context , this);
    }

    @Override
    public void onLocationChanged(Location location) {
            locationUpdate.onLocationChanged(location);
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        locationUpdate.onStatusChanged(provider, status, extras);
    }

    @Override
    public void onProviderEnabled(String provider) {
        locationUpdate.onProviderEnabled(provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        locationUpdate.onProviderEnabled(provider);
    }

    ////////////////////////////////ILocationController////////////////////////////////////


    @Override
    public void stopTracking() {
        locationState.stopTracker();
    }

    @Override
    public void startTracking() {
        locationState.startTracker();
    }

    @Override
    public void startTrackerWithMinTime(long minTime) {
        locationState.startTrackerWithMinTime(minTime);
    }

    @Override
    public void startTrackerWithMinDistance(  long minDistance) {
        locationState.startTrackerWithMinDistance(minDistance);

    }

    @Override
    public void startTrackerWithMinTimeAndDistance(  long minTime, long minDistance) {
        locationState.startTrackerWithMinTimeAndDistance(  minTime,minDistance);
    }

}
