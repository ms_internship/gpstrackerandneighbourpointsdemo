package com.example.pisoo.raaye7;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.mobilestudio.gpstracker.ILocationController;
import android.mobilestudio.gpstracker.LocationController;
import android.mobilestudio.gpstracker.LocationUpdateListener;
import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerClickedListener;
import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerLocationUpdateListener;
import android.mobilestudio.neighbourspoints.CallbackListener.onMarkerRegionStateChangedListener;
import android.mobilestudio.neighbourspoints.map.abstractMap.AbstractMap;
import android.mobilestudio.neighbourspoints.map.factory.NeighbourMapFactory;
import android.mobilestudio.neighbourspoints.models.MapPoint;
import android.mobilestudio.neighbourspoints.models.Region;
import android.mobilestudio.neighbourspoints.provider.PointsProvider;
import android.mobilestudio.neighbourspoints.provider.PointsProviderBuilder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        LocationUpdateListener, onMarkerClickedListener<Marker> , onMarkerLocationUpdateListener<Marker>
           , onMarkerRegionStateChangedListener<Marker> {

    private GoogleMap mMap;
    private EditText mfrom, mTo;
    private Button mTakemyCar, mRequestPickup, clear;
    private TextView mTimeText;
  //  private LinearLayout mLLTime;
    private ArrayList<LatLng> markerPoints;
    // flags to know the searching address is source or destination
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE_From = 1;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE_To = 2;
    private View mapView;
    private Date date;
    ILocationController controller;
    AbstractMap map;
    List<MapPoint> list ;
    Spinner spinner ;
    EditText lat , lng ;
    PointsProvider provider ;
    MapPoint updatedPoint = new MapPoint()  ;
    int pos  ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        getSupportActionBar().setTitle(R.string.app_name);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();

        linkTheViews();
        manageClicklisteners();
        controller = new LocationController(this, this);

        spinner= (Spinner) findViewById(R.id.spinner);
        lat  = (EditText) findViewById(R.id.et_lat);
        lng  = (EditText) findViewById(R.id.et_lng);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                lat.setText(String.valueOf(list.get(position).getLatLn().latitude));
                lng.setText(String.valueOf(list.get(position).getLatLn().longitude));
                pos =  position ;
               // updatedPoint = list.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_From) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                mfrom.setText(place.getName());
                mfrom.setTextColor(Color.BLACK);
                LatLng latLng = place.getLatLng();
                clearTheScreen();
                drawRoutes(latLng);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.e("TAG", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_To) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                mTo.setText(place.getName());
                mTo.setTextColor(Color.BLACK);
                LatLng latLng = place.getLatLng();
                // mMap.addMarker(new MarkerOptions().position(sydney).title(place.getName().toString()));
                drawRoutes(latLng);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                //Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }

        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        list = new ArrayList<>();

        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        list = getListOfPoints() ;


        map = new NeighbourMapFactory<GoogleMap>().create(mMap);

        // provider = new PointsProvider(new LatLng(30,30),70000);

        provider = new PointsProviderBuilder(map, list , new Region(new LatLng(30,30),70000))
                                                    .setClickedListener(this)
                                                    .setUpdateListener(this)
                                                    .setChangeStateListener(this)
                                                    .setMinTimeforUpdate(5 * 1000 * 60)
                                                    .setMinDistanceForUpdate(100).build();


         mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng userLocation = new LatLng(30.0000, 30.000);

        mMap.addMarker(new MarkerOptions().position(userLocation).title("Cairo"));
        // marker.remove();
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 9));
        markerPoints.add(userLocation);
        //mMap.clear();
        // show The Traffic Data
        mMap.setTrafficEnabled(true);

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                drawRoutes(latLng);
                getAddress(latLng);
            }
        });


         if (ContextCompat.checkSelfPermission(this.getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    // check if The GPS is Opened or not
                    if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        mMap = googleMap;
                        LatLng latLng = new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
                        clearTheScreen();
                        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(latLng, 12);
                        mMap.animateCamera(location);
                        drawRoutes(latLng);
                    } else {
                        // release Alart to open The GPS
                        buildAlertMessageNoGps();
                    }
                    return true;
                }
            });

            if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
                // Get the button view
                View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                // and next place it, on bottom right (as Google Maps app)
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                        locationButton.getLayoutParams();
                // position on right bottom
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
                layoutParams.setMargins(0, 0, 30, 30);
            }
        } else {
            Toast.makeText(this, "Your GPS is Closed .", Toast.LENGTH_SHORT).show();
        }
    }

    private void drawRoutes(LatLng latLng) {

        if (markerPoints.size() > 1) {
            clearTheScreen();
        }
        // Adding new item to the ArrayList
        markerPoints.add(latLng);
        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();

        // Setting the position of the marker
        options.position(latLng);
        if (markerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            mfrom.setText(getAddress(latLng));
            mfrom.setTextColor(Color.BLACK);
        } else if (markerPoints.size() >= 2) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

            mTo.setText(getAddress(latLng));
            mTo.setTextColor(Color.BLACK);
        }
        // Add new marker to the Google Map Android API V2
        mMap.addMarker(options);
        // Checks, whether start and end locations are captured
        if (markerPoints.size() >= 2) {
            LatLng origin = markerPoints.get(markerPoints.size() - 2);
            LatLng dest = markerPoints.get(markerPoints.size() - 1);
            // Getting URL to the Google Directions AP
            String url = getDirectionsUrl(origin, dest);
            DownloadTask downloadTask = new DownloadTask();
            // Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }
    }

    private void clearTheScreen() {
        // markerPoints.clear();
        // mMap.clear();
      //  mLLTime.animate().alpha(0.0f).setDuration(2000);
        mfrom.setText("");
        mTo.setText("");
    }

    @Override
    public void onLocationChanged(Location location) {

        // LatLng updatedPoint  = new LatLng(lo)
        drawRoutes(new LatLng(location.getLatitude(), location.getLongitude()));
        Toast.makeText(this, "updated", Toast.LENGTH_SHORT).show();

        provider.changeCenterOfRegion(new LatLng(location.getLatitude(), location.getLongitude()));
        Log.v("Location Updated ", " Hey ");

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, provider, Toast.LENGTH_SHORT).show();
        Log.v("The provider is ", provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, provider, Toast.LENGTH_SHORT).show();
        Log.v("The provider is ", provider);
    }


    @Override
    public void onMarkerClickListener(Marker marker) {
        marker.setTitle(marker.getPosition().toString());
        marker.showInfoWindow();
    }

    @Override
    public void onMarkerLocationUpdated(Marker marker) {
        marker.setTitle("I'm Updated ");
        marker.showInfoWindow();
    }

    @Override
    public void onPointEntered(Marker marker) {
        marker.setTitle("I'm in  now ");
        marker.showInfoWindow();
    }

    @Override
    public void onPointLeaved(Marker marker)   {

    }

    private class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions;
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = result.get(i);
                for (int j = 0; j < path.size(); j++) {

                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    points.add(new LatLng(lat, lng));
                }
                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.geodesic(true);
                lineOptions.color(getRouteColor(i));
                // Drawing polyline in the Google Map for the i-th route
                mMap.addPolyline(lineOptions);
            }
        }
    }

    private int getRouteColor(int i) {
        return ((i == 0) ? Color.BLUE : (i == 1) ? Color.GRAY : (i == 2) ? Color.GREEN : Color.MAGENTA);
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Sensor enabled
        String sensor = "sensor=false";
        //set units with metric
        String units = "units=metric";
        // alternatives enabled to get Multi Route
        String alternatives = "alternatives=false";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + units + "&" + alternatives;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private String getAddress(LatLng latLng) {
        if (latLng == null)
            return "";
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String finalAddress = address + " ";
            if (city != null)
                finalAddress += city + " ";
            if (state != null)
                finalAddress += state;
            return finalAddress;
        } catch (IOException io) {
        }
        return "";
    }

    private void linkTheViews() {
        date = new Date();
        markerPoints = new ArrayList<LatLng>();
      //  mLLTime = (LinearLayout) findViewById(R.id.ll_time);
     //   mLLTime.animate().alpha(0.0f);
       // mTimeText = (TextView) findViewById(R.id.tv_TimeText);
        mfrom = (EditText) findViewById(R.id.dt_from);
        mTo = (EditText) findViewById(R.id.dt_To);
        mTakemyCar = (Button) findViewById(R.id.bt_TakeMyCar);
        mTakemyCar.setText("Start Tracking ");
        mRequestPickup = (Button) findViewById(R.id.bt_RequestPickup);
        mRequestPickup.setText("Stop Tracking");
        clear = (Button) findViewById(R.id.bt_clr);


    }

    private void manageClicklisteners() {
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double llat= Double.parseDouble(lat.getText().toString());
                double llng= Double.parseDouble(lng.getText().toString());
               // LatLng latLng =  updatedPoint.getLatLn()
                 updatedPoint = new MapPoint()  ;
                 updatedPoint.latLn(new LatLng(llat ,llng));
                 updatedPoint.icon(list.get(pos).getIcon());
                 updatedPoint.setUniqueID(list.get(pos).getUniqueID());
                // float distance = provider.getDistanceBtnTwoPoints(updatedPoint.getLatLn() ,list.get(pos).getLatLn());
                 provider.OnMarkerLocationUpdated(updatedPoint);
                 list.get(pos).latLn(updatedPoint.getLatLn());
                //map.setPoints(updatePointFromList(list));
            }
        });

        mTakemyCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // getTripTime();
                controller.startTracking();
                // controller.startTrackerWithMinDistance(2);
                // controller.startTrackerWithMinTime(2000);
                controller.startTrackerWithMinTimeAndDistance(30000, 3);
                Toast.makeText(MapsActivity.this, "Started", Toast.LENGTH_SHORT).show();


            }
        });
        mRequestPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.stopTracking();
                Toast.makeText(MapsActivity.this, "Stopped", Toast.LENGTH_SHORT).show();
                //
            }
        });
        // the user will select a destination of the trip
        mTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                            .setCountry("EG")
                            .build();
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .setFilter(typeFilter)
                                    .build(MapsActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE_To);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                    Log.v("Error ", "GooglePlayServicesRepairableException");
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                    Log.v("Error ", "GooglePlayServicesNotAvailableException");
                }
            }
        });
        // the user will select a source of the trip
        mfrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                            .setCountry("EG")
                            .build();
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .setFilter(typeFilter)
                                    .build(MapsActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE_From);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                    Log.e("Error ", "GooglePlayServicesRepairableException");
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                    Log.e("Error ", "GooglePlayServicesNotAvailableException");
                }
            }
        });
    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.AlartMessageNoGPS)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private List<MapPoint> getListOfPoints() {
        List<MapPoint> points = new ArrayList<>();
        for (int i = 0; i < 5; i++) {

            MapPoint point = new MapPoint();
            Random rand = new Random();
            double lat = rand.nextDouble() + 29.00099;
            double lng = rand.nextDouble() + 29.009999;
            point.latLn(new LatLng(lat, lng));
            points.add(point);
        }
        return points;
    }

    private List<MapPoint> updatePointFromList(List<MapPoint> list) {
        Random rand = new Random();
        int index = Math.abs(rand.nextInt(2));
        for (int i =0 ;i< list.size();i++)
        {
            if(i==index){
                Log.v("PrePoint " , list.get(i).getLatLn().toString());
                list.remove(i) ;
               list.set(i,movepoint(list.get(i))) ;
                Log.v("postPoint " , list.get(i).getLatLn().toString());

                //list.remove(i+1);
            }
        }
        return list ;
    }

    private MapPoint movepoint(MapPoint point) {
        double lat = point.getLatLn().latitude + 0.0001;
        double lng = point.getLatLn().longitude + 0.0001;
        point.latLn(new LatLng(lat, lng));
        return point;
    }

}
