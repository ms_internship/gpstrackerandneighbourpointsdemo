package android.mobilestudio.gpstracker;


interface ILocationStates {
    void startTracker();

    void startTrackerWithMinTime(long minTime);

    void startTrackerWithMinDistance(long minDistance);

    void startTrackerWithMinTimeAndDistance(long minTime, long minDistance);

    void stopTracker();
}
