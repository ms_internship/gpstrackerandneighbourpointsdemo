package android.mobilestudio.gpstracker;

import android.location.Location;
import android.os.Bundle;

/**
 * Created by pisoo on 8/13/2017.
 */

public interface LocationUpdateListener {
    // give the activity all function of location listener Or not ?
    void onLocationChanged(Location location);

    void onStatusChanged(String provider, int status, Bundle extras);

    void onProviderEnabled(String provider);

    void onProviderDisabled(String provider);
}
